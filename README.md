# ELK

### Prerequisites

docker

### Deploy

#### Elasticsearch
cd esdata/

docker run -d --name elasticsearch  -p 9200:9200 -p 9300:9300 -v "$PWD":/usr/share/elasticsearch/data elasticsearch

#### Logstash
cd logstash/

docker run -d --name logstash -p 5044:5044 --link elasticsearch:elasticsearch -v "$PWD":/logstash logstash -f /logstash/logstash.conf

#### Kibana

docker run --name kibana --link elasticsearch:elasticsearch -p 5601:5601 -d kibana

#### Send logs

nc localhost 5044 < "logFile"

#### Access Kibana

If all the containers are up and running as expected, visit the following URL to access the web interface.
http://localhost:5601/

### Other information 

(deploy)
https://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/how-to-run-elk-stack-on-docker-container.html

(input filter and output logstash)
http://knes1.github.io/blog/2015/2015-08-16-manage-spring-boot-logs-with-elasticsearch-kibana-and-logstash.html


